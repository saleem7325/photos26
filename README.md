**Pauli Peralta and Saleem Khan**

## Brief Introduction:

This is a program that utilizes Android Studio and Java to allow users to sign in and store JPEG, PNG, and GIF files. In addition, users are able to tag their files and filter
through their albums using AND and OR conditions with single tags or a combincation.

## Functions

### Creating an album
To create an album click on the 'Create album' button. This will prompt you to provide the name of your new album and save it.

### Deleting an album
To delete an album you must click on it first. Once you click on an album, the 'Delete album' button will become accessible and you can click it to delete the selected album.

### Opening an album
To open an album you must click on it first. Once you click on an album, the 'Open album' button will become accessible and you can then click on the button to open up the specified album.

*  **Adding to an album** <br /> Once you open an album, you can add photos by clicking on the 'Add photo' button. The button will prompt you to choose a file from you system to upload. 
	Note that you can only upload photos ending with the following extensions: **BMP, GIF, JPEG, and PNG**. Once you select a file, you will be prompted to provide a caption for your photo, which is 
	optional, and save your selection. <br /><br />
* **Slideshow** <br />
      A slideshow of an album is only possible when there are photos present. If an 
      album does not have any photos, then the slideshow option will remain 
      disabled. <br /><br />
* **Others** <br />
      Every other functionality that you will see when you open an album will 
      be self-explanatory due to the name of the buttons. However, it is 
      important to note that you must **click on a photo** in order to make 
      the other button/functionalities accessible. Unless you click on a 
      photo, the other functionalities will remain disabled. <br /><br />
### Editing an album
By clicking on an album, you will be able to click on the 'Edit album' button, which will allow you do rename the album.

### Search
Search is only accessible when there is at at least one album with one photo. Unless that condition is met, the search functionality is unavailable, since there wouldn't be anything to search through.<br />

* **Single-tag search** <br />
	Once there is an album with at least one picture, one of the methods you will be able to filter your images by is a single-tag search. You will be prompted to enter a name-value pair, indicating the 
	specific tags you want to filter your images by. If the search yields a result, you will be able to make an album of the resulting set of photos, else this option remains inaccessible. <br /><br />

* **Combination-tag search** <br />
	Once there is an album with at least one picture, one of the methods you will be able to filter your images by is a tag-combination search. You will be prompted to provide TWO tags and to choose whether you 
	want the tags to be conjuctive or disjunctive. This will then return a set of photos that match the tags indicated. 

### Quiting/Login off
You can quit or logout at anytime by pressing the respective buttons at the bottom of the screen.