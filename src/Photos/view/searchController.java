package Photos.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import Photos.app.Album;
import Photos.app.Photo;
import Photos.app.User;
import Photos.app.Userbase;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * 
 * @author Pauli Peralta
 * @author Saleem Khan
 *
 */
public class searchController {

	private @FXML Button filterByTags;
	private @FXML Button filterByDate;
	private @FXML Button createAlbum;
	private @FXML Button quit;
	private @FXML Button signOut;
	private @FXML TilePane tile = new TilePane();
	/**
	 * stores the result for the search
	 */
	private List<Photo> matches = null;

	/**
	 * filters photos by date
	 * 
	 * @param e ActionEvent to handle when user clicks on button to filter by date
	 * @throws IOException Throws exception if file can't be read
	 */
	public void filterByDate(ActionEvent e) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Filtering by date");
		alert.setHeaderText("You must include ATLEAST ONE date. The indicated dates are inclusive");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField startDate = new TextField();
		startDate.setPromptText("MM/DD/YYYY");
		TextField endDate = new TextField();
		endDate.setPromptText("MM/DD/YYYY");

		grid.add(new Label("Start Date"), 0, 0);
		grid.add(startDate, 1, 0);
		grid.add(new Label("End Date"), 0, 1);
		grid.add(endDate, 1, 1);

		alert.getDialogPane().setContent(grid);

		ButtonType searchButton = new ButtonType("Search", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(searchButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == searchButton) {

			Calendar c = Calendar.getInstance();
			int presentYear = c.get(Calendar.YEAR);

			boolean startDateIndicated = startDate.getText().trim().isEmpty() ? false : true;
			boolean endDateIndicated = endDate.getText().trim().isEmpty() ? false : true;

			if (!startDateIndicated && !endDateIndicated) {
				displayErrorMessage("Search unsuccessful!\n\nNo date(s) indicated.", "Invalid Dates");
				return;
			}

			boolean startDateValid = startDateIndicated ? isValid(startDate.getText()) : false;
			boolean endDateValid = endDateIndicated ? isValid(endDate.getText()) : false;

			if ((startDateIndicated && !startDateValid) || (endDateIndicated && !endDateValid)) {
				displayErrorMessage("Search unsuccessful!\n\nAll dates must follow the format: MM/DD/YYYY"
						+ "\n\n- MM must bet between 1 and 12"
						+ "\n- YYYY must be a positive integer that doesn't exceed the present year: " + presentYear
						+ "\n-DD must must be at least 1 and cannot exceed the following integer depending on the MM: "
						+ "\n\nMM - DD" + "\n1 - 31" + "\n2 - 28 or 29 if leap year" + "\n3 - 31" + "\n4 - 30"
						+ "\n5 - 31" + "\n6 - 30" + "\n7 - 31" + "\n8 - 31" + "\n9 - 30" + "\n10 - 31" + "\n11 - 30"
						+ "\n12 - 31", "Incorrect Date Format");
				return;
			}

			LocalDate start = null;
			LocalDate end = null;
			String[] parse;
			String year, month, day;

			if (startDateIndicated) {
				parse = startDate.getText().split("/");

				year = parse[2];
				month = parse[0];
				day = parse[1];

				start = LocalDate.parse(year + "-" + month + "-" + day);
			}

			if (endDateIndicated) {
				parse = endDate.getText().split("/");

				year = parse[2];
				month = parse[0];
				day = parse[1];

				end = LocalDate.parse(year + "-" + month + "-" + day);
			}

			if (start != null && end != null && start.isAfter(end)) {
				displayErrorMessage("Search unsuccessful!\nStart day must come before the end date.",
						"Incorrect Date Range");
				return;
			}

			getMatches(start, end);
		}

	}

	/**
	 * Gets all pictures that fall within the date range indicated
	 * 
	 * @param start Start date by which to start filtering
	 * @param end   End date by which to end filtering
	 * @throws FileNotFoundException Throws exception if no file found
	 */
	private void getMatches(LocalDate start, LocalDate end) throws FileNotFoundException {
		User currentUser = Userbase.getCurrentUser();
		Set<Photo> matchesFound = new HashSet<>();

		for (Album album : currentUser.getAlbums()) {
			for (Photo photo : album.getPhotos()) {
				if (start == null) {
					if (!photo.getTime().isAfter(end))
						matchesFound.add(photo);
				} else if (end == null) {
					if (!photo.getTime().isBefore(start))
						matchesFound.add(photo);
				} else {
					if (!photo.getTime().isBefore(start) && !photo.getTime().isAfter(end))
						matchesFound.add(photo);
				}

			}
		}

		if (!matchesFound.isEmpty()) {
			matches = new ArrayList<>();
			matches.addAll(matchesFound);
		} else
			matches = null;

		displayMatches();

	}

	/**
	 * Checks if a string is a valid date in the format of MM/DD/YYYY
	 * 
	 * @param date Input to be parsed
	 * @return Returns true if correctly formatted, false otherwise
	 */
	private boolean isValid(String date) {
		if (date.trim().isEmpty())
			return false;
		Calendar c;
		int[] days_of_month = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		c = Calendar.getInstance();
		int presentYear = c.get(Calendar.YEAR);

		String[] parse = date.split("/");
		if (parse.length != 3)
			return false;

		int month, day, year;

		try {
			month = Integer.parseInt(parse[0]);
			day = Integer.parseInt(parse[1]);
			year = Integer.parseInt(parse[2]);
		} catch (NumberFormatException nfe) {
			return false;
		}

		if (parse[2].length() < 4 || parse[0].length() < 2 || parse[1].length() < 2)
			return false;

		if (year <= 0 || year > presentYear)
			return false;

		boolean leapYear = isLeapYear(year);
		if (leapYear)
			days_of_month[1] = 29;

		if (month < 1 || month > 12)
			return false;

		int lastDayOfMonth = days_of_month[month - 1];
		if (day < 1 || day > lastDayOfMonth)
			return false;
		return true;
	}

	/**
	 * Determines if a year is a leap year
	 * 
	 * @param year year
	 * @return boolean returns true if leap year else false
	 */
	private boolean isLeapYear(int year) {
		boolean isLeapYear = false;

		if (year % 4 == 0) {

			// if the year is century
			if (year % 100 == 0) {

				// if year is divided by 400
				// then it is a leap year
				if (year % 400 == 0)
					isLeapYear = true;
				else
					isLeapYear = false;
			}

			// if the year is not century
			else
				isLeapYear = true;
		} else
			isLeapYear = false;

		return isLeapYear;
	}

	/**
	 * search by tags specified
	 * 
	 * @param e user clicks filter by tags button
	 * @throws IOException invalid user interaction
	 */
	public void filterByTags(ActionEvent e) throws IOException {
		List<String> choices = new ArrayList<>();
		choices.add("Filter By Single Tag");
		choices.add("Filter By Tag Combination");

		ChoiceDialog<String> dialog = new ChoiceDialog<>(choices.get(0), choices);
		dialog.setTitle("Move Photo");
		dialog.setHeaderText("Choose an option or press 'Cancel'");
		dialog.setContentText("Select a filter method:");

		ButtonType nextButton = new ButtonType("Next", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().setAll(nextButton, ButtonType.CANCEL);

		Optional<String> result = dialog.showAndWait();

		if (result.isPresent()) {
			String filterType = result.get();
			if (filterType.equals("Filter By Single Tag"))
				singleTagFilter();
			else
				AND_or_OR();
		}

	}

	/**
	 * Creates an album based on search results
	 * 
	 * @param e Users clicks create album method
	 * @throws IOException Invalid user interaction
	 */
	public void createAlbum(ActionEvent e) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Creating album");
		alert.setHeaderText("Click 'Save' to store any changes.");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField albumName = new TextField();
		albumName.setPromptText("Album name");

		grid.add(new Label("Album name:"), 0, 1);
		grid.add(albumName, 1, 1);
		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Save", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == saveButton) {
			if (Userbase.getCurrentUser().getAlbum(albumName.getText()) == null) {
				Userbase.getCurrentUser().addAlbum(albumName.getText());
				Album newAlbum = Userbase.getCurrentUser().getAlbum(albumName.getText());

				for (Photo photo : matches)
					newAlbum.addPhoto(photo);
				matches = null;
				tile.getChildren().clear();
				setState();
				displayConfirmationMessage("Album was successfully created", "Success!");
			} else {
				displayErrorMessage("Album name already exists. Duplicate albums are not allowed",
						"Duplicate Album Error");
			}

		}
	}

	/**
	 * Displays previous screen
	 * 
	 * @param e User clicks back button
	 * @throws IOException Invalid user interaction
	 */
	public void back(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/nonAdmin.fxml"));
		Pane root = (Pane) loader.load();

		nonAdminController n = loader.getController();
		n.displayAlbums();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
		root.requestFocus();
	}

	/**
	 * Logs out user from application
	 * 
	 * @param e User clicks log out button
	 * @throws IOException Invalid user interaction
	 */
	public void logOut(ActionEvent e) throws IOException {
		Userbase.save();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/login.fxml"));
		Pane root = (Pane) loader.load();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Quits application
	 * 
	 * @param e user clicks quit button
	 * @throws IOException Invalid user interation
	 */
	public void quit(ActionEvent e) throws IOException {
		Userbase.save();
		Platform.exit();
	}

	/**
	 * disables buttons based on search results
	 */
	private void setState() {
		if (matches == null) {
			createAlbum.setDisable(true);
		} else
			createAlbum.setDisable(false);
	}

	/**
	 * initiates search based on single tag
	 * 
	 * @throws FileNotFoundException file not found
	 */
	private void singleTagFilter() throws FileNotFoundException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Filtering by single tag");
		alert.setHeaderText("Click 'Search' to display matches");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField name = new TextField();
		name.setPromptText("Name");
		TextField value = new TextField();
		value.setPromptText("Value");

		grid.add(new Label("Name:"), 0, 0);
		grid.add(name, 1, 0);
		grid.add(new Label("Value:"), 0, 1);
		grid.add(value, 1, 1);

		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Search", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.isPresent())
			getMatches(name.getText(), value.getText());
	}

	/**
	 * Gets matches for specific key value pair
	 * 
	 * @param name  key
	 * @param value value
	 * @throws FileNotFoundException file not found
	 */
	private void getMatches(String name, String value) throws FileNotFoundException {
		User currentUser = Userbase.getCurrentUser();
		Set<Photo> matchesFound = new HashSet<>();

		for (Album album : currentUser.getAlbums()) {
			for (Photo photo : album.getPhotos()) {
				if (photo.findTag(name, value)) {
					matchesFound.add(photo);
				}
			}
		}

		if (!matchesFound.isEmpty()) {
			matches = new ArrayList<>();
			matches.addAll(matchesFound);
		} else
			matches = null;

		displayMatches();
	}

	/**
	 * gets matches based on 2 key value pairs and filter
	 * 
	 * @param name   key
	 * @param value  value
	 * @param name2  key2
	 * @param value2 value
	 * @param filter and or or
	 * @throws FileNotFoundException
	 */
	private void getMatches(String name, String value, String name2, String value2, String filter)
			throws FileNotFoundException {
		User currentUser = Userbase.getCurrentUser();
		Set<Photo> matchesFound = new HashSet<>();

		for (Album album : currentUser.getAlbums()) {
			for (Photo photo : album.getPhotos()) {
				if (filter.equals("Conjunctive -- AND")) {
					if (photo.findTag(name, value) && photo.findTag(name2, value2))
						matchesFound.add(photo);
				} else {
					if (photo.findTag(name, value) || photo.findTag(name2, value2))
						matchesFound.add(photo);
				}
			}
		}

		if (!matchesFound.isEmpty()) {
			matches = new ArrayList<>();
			matches.addAll(matchesFound);
		} else
			matches = null;

		displayMatches();
	}

	/**
	 * displays the matches
	 * 
	 * @throws FileNotFoundException file not found
	 */
	private void displayMatches() throws FileNotFoundException {
		setState();
		tile.getChildren().clear();
		if (matches == null)
			return;

		for (Photo photo : matches) {
			File thumbnail;

			thumbnail = new File(photo.getPath());
			Image image = new Image(new FileInputStream(thumbnail), 100, 100, true, true);
			ImageView imageView = new ImageView(image);

			VBox v = new VBox(5);
			v.getChildren().addAll(imageView, new Text(photo.getCaption()));
			tile.getChildren().add(v);
			tile.setHgap(20);
			tile.setVgap(20);
		}

	}

	/**
	 * Prompts user for both key values
	 * 
	 * @param type and or or
	 * @throws FileNotFoundException file not found
	 */
	private void comboTagFilter(String type) throws FileNotFoundException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Filtering by single tag");
		alert.setHeaderText("Click 'Search' to display matches");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField name = new TextField();
		name.setPromptText("REQUIRED");
		TextField value = new TextField();
		value.setPromptText("REQUIRED");
		TextField name2 = new TextField();
		name2.setPromptText("REQUIRED");
		TextField value2 = new TextField();
		value2.setPromptText("REQUIRED");

		grid.add(new Label("Name:"), 0, 0);
		grid.add(name, 1, 0);
		grid.add(new Label("Value:"), 0, 1);
		grid.add(value, 1, 1);

		grid.add(new Label("Name:"), 0, 3);
		grid.add(name2, 1, 3);
		grid.add(new Label("Value:"), 0, 4);
		grid.add(value2, 1, 4);

		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Search", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == saveButton) {
			if (name.getText().trim().isEmpty() || name2.getText().trim().isEmpty() || value2.getText().trim().isEmpty()
					|| value.getText().trim().isEmpty()) {
				displayErrorMessage("No field can be left empty!", "Missing Fields Error");

			} else
				getMatches(name.getText(), value.getText(), name2.getText(), value2.getText(), type);

		}

	}

	/**
	 * prompts user to choose and or or
	 * 
	 * @throws FileNotFoundException file not found
	 */
	private void AND_or_OR() throws FileNotFoundException {
		List<String> choices = new ArrayList<>();
		choices.add("Conjunctive -- AND");
		choices.add("Disjunctive -- OR");

		ChoiceDialog<String> dialog = new ChoiceDialog<>(choices.get(0), choices);
		dialog.setTitle("Filter type");
		dialog.setHeaderText("Choose an option or press 'Cancel'");
		dialog.setContentText("Select a filter method:");

		ButtonType nextButton = new ButtonType("Next", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().setAll(nextButton, ButtonType.CANCEL);

		Optional<String> result = dialog.showAndWait();

		if (result.isPresent()) {
			String filterType = result.get();
			comboTagFilter(filterType);
		}

	}

	/**
	 * Initilizes screen with create album button disabled
	 */
	public void init() {
		createAlbum.setDisable(true);
	}

	/**
	 * Dislays an error message with specified message and title
	 * 
	 * @param message message to display
	 * @param title   title of window
	 */
	private void displayErrorMessage(String message, String title) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

	}

	/**
	 * Displays confirmation with specified message and title
	 * 
	 * @param message message to displays
	 * @param title   title of window
	 */
	private void displayConfirmationMessage(String message, String title) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

	}
}
