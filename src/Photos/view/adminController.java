package Photos.view;

import java.io.IOException;
import java.util.Optional;

import Photos.app.User;
import Photos.app.Userbase;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.Node;

/**
 * 
 * @author Pauli Peralta
 * @author Saleem Khan
 *
 */

public class adminController {

	private @FXML ListView<User> userList;
	private @FXML Button quit;
	private @FXML Button signOut;
	private ObservableList<User> users;
	
	/**
	 * When admin clicks on the button to display the list of all users, this method gets the current list 
	 * of active users and displays it on a list view.
	 * 
	 * @param e
	 * AciotnEvent argument for when admin clicks on the 'list all users' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */

	public void listUsers(ActionEvent e) throws IOException {
		users = FXCollections.observableArrayList();
		users.addAll(Userbase.getActiveUsers());
		userList.setItems(users);

	}

	/**
	 * Handles when admin clicks on the button to delete a user.
	 * 
	 * @param e
	 * AciotnEvent argument for when admin clicks on the 'delete user' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */
	public void removeUser(ActionEvent e) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Deleting User");
		alert.setHeaderText("Enter a username below and click 'save' to delete the user from the database");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField newUser = new TextField();
		newUser.setPromptText("Username");

		grid.add(new Label("Username:"), 0, 1);
		grid.add(newUser, 1, 1);
		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Save", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();


		if(result.get() == saveButton) {
			String username = newUser.getText();
			Userbase.removeUser(username);
		}

	}

	/**
	 * Handles when admin clicks on the button to create a new user.
	 * 
	 * @param e
	 * AciotnEvent argument for when admin clicks on the 'create user' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */
	public void createUser(ActionEvent e) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Creating New User");
		alert.setHeaderText("Enter a username below and click 'save' to add the user to the database");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField newUser = new TextField();
		newUser.setPromptText("Username");

		grid.add(new Label("Username:"), 0, 1);
		grid.add(newUser, 1, 1);
		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Save", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();


		if(result.get() == saveButton) {
			String username = newUser.getText();
			Userbase.addUser(username);
		}
	}
	
	/**
	 * Handles when admin clicks on the button to logout.
	 * 
	 * @param e
	 * AciotnEvent argument for when admin clicks on the 'logout' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */
	public void logOut(ActionEvent e) throws IOException {
		Userbase.save();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/login.fxml"));
		Pane root = (Pane) loader.load();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Handles when admin clicks on the quit to logout.
	 * 
	 * @param e
	 * AciotnEvent argument for when admin clicks on the 'quit' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */
	public void quit(ActionEvent e) throws IOException {
		Userbase.save();
		Platform.exit();
	}

}
