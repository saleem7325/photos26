package Photos.view;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.stage.FileChooser.ExtensionFilter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import Photos.app.Album;
import Photos.app.Photo;
import Photos.app.Userbase;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;

/**
 * 
 * @author Pauli Peralta
 * @author Saleem Khan
 *
 */

public class photoGalleryController {

	/**
	 * This class is just for Testing, I took away all functionality from user login
	 * page and only have add photo button which prompts user to select a photo
	 * which is then displayed on the scrollpane This class pertains to
	 * userlogintesting.fxml
	 */
	private @FXML Button addPhoto;
	private @FXML Button editCaption;
	private @FXML Button deletePhoto;
	private @FXML Button displayPhoto;
	private @FXML Button copyPhoto;
	private @FXML Button movePhoto;
	private @FXML Button slideShow;
	private @FXML Button addTag;
	private @FXML Button removeTag;
	private @FXML Button quit;
	private @FXML Button signOut;
	private @FXML ScrollPane scrollPane;
	private @FXML TilePane tile = new TilePane();
	private @FXML Label photoSelectedName;

	/**
	 * Stores the photo that the user selects
	 */
	private Photo photoSelected;

	/**
	 * Current album being displayed
	 */
	private Album currentAlbum;

	/**
	 * Sets the album to be displayed
	 * 
	 * @param album album to display
	 */
	public void setCurrentAlbum(Album album) {
		currentAlbum = album;
	}

	/**
	 * prompts user to choose photo
	 * 
	 * @return File returns file that was selected
	 */
	private File getFile() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Adding New Photo");
		alert.setHeaderText("Please select a file");

		ButtonType chooseFile = new ButtonType("Choose file");
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(chooseFile, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == chooseFile) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters()
					.addAll(new ExtensionFilter("Image Files", "*.png", "*.jpeg", "*.gif", "*.bmp"));
			fileChooser.setTitle("Select Photo");
			File photo = fileChooser.showOpenDialog(loginController.stage);
			return photo;
		}

		return null;
	}

	/**
	 * Prompts user to input a caption
	 * 
	 * @param file file to add caption to
	 * @return String returns caption that was added
	 */
	private String getCaption(File file) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Adding New Photo");
		alert.setHeaderText("Add a caption!");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField captionInput = new TextField();
		captionInput.setPromptText("OPTIONAL");

		grid.add(new Label("Caption:"), 0, 1);
		grid.add(captionInput, 1, 1);
		alert.getDialogPane().setContent(grid);
		grid.add(new Label("File:"), 0, 0);
		grid.add(new Label(file.getAbsolutePath()), 1, 0);

		ButtonType saveButton = new ButtonType("Save", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result2 = alert.showAndWait();

		if (result2.get() == saveButton)
			return captionInput.getText();
		return null;

	}

	/**
	 * Checks if a photo is already in album
	 * 
	 * @param filePath path of file
	 * @return Photo photo if its in album else null
	 */
	private Photo getExistingInstance(String filePath) {
		for (Album albums : Userbase.getCurrentUser().getAlbums()) {
			for (Photo currPhoto : albums.getPhotos()) {
				if (currPhoto.getPath().equals(filePath))
					return currPhoto;
			}
		}

		return null;
	}

	/**
	 * adds a photo to album
	 * 
	 * @param e user clicks add photo button
	 * @throws IOException Invalid user interaction
	 */
	public void addPhoto(ActionEvent e) throws IOException {
		File file = getFile();
		if (file == null)
			return;
		String caption = getCaption(file);
		if (caption == null)
			return;

		if (currentAlbum.getPhoto(file.getAbsolutePath()) != null) {
			displayErrorMessage("Photo already exists. Duplicate photos in an album are not allowed",
					"Duplicate Photo Error");
			return;
		}

		Photo photoToAdd = getExistingInstance(file.getAbsolutePath());
		// if photo not found in another album, make new instance + update caption
		if (photoToAdd == null)
			photoToAdd = new Photo(file.getAbsolutePath(), caption);
		else
			photoToAdd.setCaption(caption);

		currentAlbum.addPhoto(photoToAdd);
		Userbase.save();
		displayPhotos();
	}

	/**
	 * Deletes photo from album
	 * 
	 * @param e user clicks delete photo button
	 * @throws IOException Invalid user interaction
	 */
	// done
	public void deletePhoto(ActionEvent e) throws IOException {
		Userbase.getCurrentUser().getAlbum(currentAlbum.getName()).removePhoto(photoSelected);
		photoSelected = null;
		Userbase.save();
		displayPhotos();
	}

	/**
	 * edits the caption of a photo in album
	 * 
	 * @param e user clicks edit caption button
	 * @throws IOException Invalid user interation
	 */
	public void editCaption(ActionEvent e) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Editing Caption");
		alert.setHeaderText("Click 'Save' to store any changes.");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField newCaption = new TextField();
		newCaption.setText(photoSelected.getCaption());

		grid.add(new Label("Caption:"), 0, 1);
		grid.add(newCaption, 1, 1);
		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Save", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == saveButton)
			photoSelected.setCaption(newCaption.getText());
		Userbase.save();
		displayPhotos();
	}

	/**
	 * Displays the photo user clicks on
	 * 
	 * @param e User clicks display photo button
	 * @throws IOException Invalid user interaction
	 */
	public void displayPhoto(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/displayPhoto.fxml"));
		Pane root = (Pane) loader.load();

		displayPhotoController n = loader.getController();
		n.setPhoto(photoSelected);
		n.setAlbum(currentAlbum);
		n.displayCaption();
		n.displayDate();
		n.displayPhoto();
		n.displayTags();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
		stage.setOnHiding((EventHandler<WindowEvent>) new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						try {
							Userbase.save();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}
		});
		root.requestFocus();
	}

	/**
	 * adds a tag to a photo
	 * 
	 * @param e user click add tag button
	 * @throws IOException Invalid user interaction
	 */
	public void addTag(ActionEvent e) throws IOException {
		updateTags(true);
	}

	/**
	 * removes a tag from a photo
	 * 
	 * @param e user clicks remove tag button
	 * @throws IOException Invalid user interaction
	 */
	// done
	public void removeTag(ActionEvent e) throws IOException {
		if (!updateTags(false)) {
			displayErrorMessage("The name-value pair indicated was not found.", "Tag Removal Error");
		}
	}

	/**
	 * Copies photo from one album to another
	 * 
	 * @param e User clicks
	 * @throws IOException Invalid user interaction
	 */
	// done
	public void copyPhoto(ActionEvent e) throws IOException {
		move_or_copy(false);
		Userbase.save();
		displayPhotos();
	}

	/**
	 * Moves photo from one album to another
	 * 
	 * @param e User clicks move album button
	 * @throws IOException Invalid user input
	 */
	public void movePhoto(ActionEvent e) throws IOException {
		move_or_copy(true);
		Userbase.save();
		displayPhotos();
	}

	/**
	 * Displays slideshow view
	 * 
	 * @param e user clicks slideshow button
	 * @throws IOException Invalid user input
	 */
	public void slideShow(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/slideshow.fxml"));
		Pane root = (Pane) loader.load();

		SlideShowController n = loader.getController();
		n.initiate(currentAlbum);
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
		stage.setOnHiding((EventHandler<WindowEvent>) new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						try {
							Userbase.save();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}
		});
		root.requestFocus();

	}

	/**
	 * Displays previous screen
	 * 
	 * @param e user clicks back button
	 * @throws IOException Invalid user input
	 */
	public void back(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/nonAdmin.fxml"));
		Pane root = (Pane) loader.load();

		nonAdminController n = loader.getController();

		n.displayAlbums();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		// scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
		root.requestFocus();
	}

	/**
	 * Displays photos in album
	 * 
	 * @throws FileNotFoundException file not found
	 */
	public void displayPhotos() throws FileNotFoundException {
		tile.getChildren().clear();
		List<Photo> userPhotos = currentAlbum.getPhotos();
		for (Photo photo : userPhotos) {
			File thumbnail;

			if (currentAlbum.getAlbumSize() == 0)
				return;

			thumbnail = new File(photo.getPath());
			Image image = new Image(new FileInputStream(thumbnail), 100, 100, true, true);
			ImageView imageView = new ImageView(image);
			imageView.setFitHeight(100.0);
			imageView.setFitWidth(100.0);
			imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent mouseEvent) {

					if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
						photoSelected = photo;
						photoSelected();

					}
				}
			});
			VBox v = new VBox(5);
			v.getChildren().addAll(imageView, new Text(photo.getCaption()));
			tile.getChildren().add(v);
			tile.setHgap(20);
			tile.setVgap(20);
		}

		initState();
	}

	/**
	 * logs user out
	 * 
	 * @param e user clicks logout button
	 * @throws IOException Invalid user input
	 */
	public void logOut(ActionEvent e) throws IOException {
		Userbase.save();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/login.fxml"));
		Pane root = (Pane) loader.load();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * closes application for user
	 * 
	 * @param e user clicks quit
	 * @throws IOException Invalid user input
	 */
	public void quit(ActionEvent e) throws IOException {
		Userbase.save();
		Platform.exit();
	}

	/**
	 * Determines what buttons to disable
	 */
	private void initState() {
		editCaption.setDisable(true);
		deletePhoto.setDisable(true);
		displayPhoto.setDisable(true);
		addTag.setDisable(true);
		removeTag.setDisable(true);
		movePhoto.setDisable(true);
		copyPhoto.setDisable(true);
		photoSelectedName.setText("None");

		if (currentAlbum.getAlbumSize() == 0)
			slideShow.setDisable(true);
		else
			slideShow.setDisable(false);
	}

	/**
	 * Takes in true if we add a tag or false if we want to remove a tag
	 * 
	 * @param addTag true or false
	 * @return boolean true if tag was added else false
	 */
	private boolean updateTags(boolean addTag) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Adding tag");
		alert.setHeaderText("Click 'Save' to store any changes.");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField name = new TextField();
		name.setPromptText("Name");
		TextField value = new TextField();
		value.setPromptText("Value");

		grid.add(new Label("Name:"), 0, 0);
		grid.add(name, 1, 0);
		grid.add(new Label("Value:"), 0, 1);
		grid.add(value, 1, 1);

		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Save", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == saveButton) {
			if (addTag) {
				photoSelected.addTag(name.getText(), value.getText());
				return true;
			} else {

				boolean ans = photoSelected.removeTag(name.getText(), value.getText());
				return ans;
			}
		}

		return false;

	}

	/**
	 * Determines if move button was clicked or copy button was clicked
	 * 
	 * @param move true if move else copy
	 */
	private void move_or_copy(boolean move) {
		List<String> choices = new ArrayList<>();
		for (Album album : Userbase.getCurrentUser().getAlbums()) {
			if (!album.getName().equals(currentAlbum.getName()))
				choices.add(album.getName());
		}

		String msg;
		if (choices.isEmpty())
			msg = "No other albums available";
		else
			msg = choices.get(0);

		ChoiceDialog<String> dialog = new ChoiceDialog<>(msg, choices);
		dialog.setTitle("Move Photo");
		dialog.setHeaderText("Select an album from the dropdown menu below.");
		dialog.setContentText("Choose where to move photo:");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			if (msg.equals("No other albums available"))
				return;
			String albumName = result.get();
			Album destinationAlbum = Userbase.getCurrentUser().getAlbum(albumName);
			if (destinationAlbum.getPhoto(photoSelected.getPath()) != null) {
				displayErrorMessage(
						"Photo already exists in the destination album. Duplicate photos in albums are not allowed",
						"Duplicate Photo Error");
				return;
			} else {
				if (move)
					currentAlbum.removePhoto(photoSelected);
				destinationAlbum.addPhoto(photoSelected);
			}
		}

	}

	/**
	 * Determines what photo is selected
	 */
	private void photoSelected() {
		editCaption.setDisable(false);
		deletePhoto.setDisable(false);
		displayPhoto.setDisable(false);
		addTag.setDisable(false);
		removeTag.setDisable(false);
		movePhoto.setDisable(false);
		copyPhoto.setDisable(false);
		photoSelectedName.setText(photoSelected.getName());
	}

	/**
	 * Displays an error message with specified message and title
	 * 
	 * @param message Message to display
	 * @param title   title of window
	 */
	private void displayErrorMessage(String message, String title) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

	}

}
