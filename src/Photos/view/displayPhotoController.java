package Photos.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;

import Photos.app.Album;
import Photos.app.Photo;
import Photos.app.Tag;
import Photos.app.Userbase;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * 
 * @author Pauli Peralta
 * @author Saleem Khan
 *
 */

public class displayPhotoController {

	private @FXML Label printDate;
	private @FXML Label printCaption;
	private @FXML TableView<Tag> tagTable;
	private @FXML ImageView photoView;
	private @FXML TableColumn<Tag, String> name;
	private @FXML TableColumn<Tag, String> value;

	private Photo currentPhoto;
	private Album album;


	/**
	 * Sets private field that keeps track of the current photo being displayed. 
	 * 
	 * @param photo
	 * Sets private field that keeps track of the current photo being displayed.
	 */
	public void setPhoto(Photo photo) {
		currentPhoto = photo;
	}

	/**
	 * Sets private field that keeps track of the current album opened. 
	 * 
	 * @param album
	 * Indicates the album instance opened.
	 */
	public void setAlbum(Album album) {
		this.album = album;
	}

	/**
	 * Displays the caption of a photo.
	 */
	public void displayCaption() {
		printCaption.setText(currentPhoto.getCaption().trim().isEmpty() ? "[NO CAPTION]" : currentPhoto.getCaption());
	}

	/**
	 * Displays the last modified date of a photo.
	 */
	public void displayDate() {
		printDate.setText(currentPhoto.getDate());
	}

	/**
	 * Displays a photo along with its tags, date, and caption
	 * 
	 * @throws FileNotFoundException
	 * Attempts to read from photo's path. Exception thrown if no path found.
	 */
	public void displayPhoto() throws FileNotFoundException {
		File picture = new File(currentPhoto.getPath());
		Image image = new Image(new FileInputStream(picture), 350, 300, true, true);
		photoView.setImage(image);
	}

	/**
	 * Displays the tags of a photo on a table view.
	 */
	public void displayTags() {
		tagTable.setPlaceholder(new Label("No tags found"));
		name.setCellValueFactory(new PropertyValueFactory<Tag, String>("name"));
		value.setCellValueFactory(new PropertyValueFactory<Tag, String>("value"));

		for (Entry<String, List<String>> tags : currentPhoto.getTags().entrySet()) {
			String name = tags.getKey();
			List<String> values = tags.getValue();

			for (String vals : values) {
				tagTable.getItems().add(new Tag(name, vals));
			}
		}

		final VBox vbox = new VBox();
		vbox.setSpacing(5);
		vbox.setPadding(new Insets(10, 0, 0, 10));
		Scene scene = new Scene(new Group());
		((Group) scene.getRoot()).getChildren().addAll(vbox);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();

	}

	/**
	 * Handles when user clicks on the button to logout.
	 * 
	 * @param e
	 * AciotnEvent argument for when user clicks on the 'logout' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */
	public void logOut(ActionEvent e) throws IOException {
		Userbase.save();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/login.fxml"));
		Pane root = (Pane) loader.load();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Handles when user clicks on the button to quit.
	 * 
	 * @param e
	 * AciotnEvent argument for when user clicks on the 'quit' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */
	public void quit(ActionEvent e) throws IOException {
		Userbase.save();
		Platform.exit();
	}

	/**
	 * Returns user to the previous screen
	 * 
	 * @param e
	 * AciotnEvent argument for when user clicks on the 'go back' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */
	public void back(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/photoGallery.fxml"));
		Pane root = (Pane) loader.load();

		photoGalleryController n = loader.getController();
		n.setCurrentAlbum(album);
		n.displayPhotos();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
		root.requestFocus();
	}

}
