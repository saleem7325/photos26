package Photos.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

import java.io.IOException;

import Photos.app.Album;
import Photos.app.Photo;
import Photos.app.Userbase;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * @author Pauli Peralta
 * @author Saleem Khan
 */
public class nonAdminController {

	private @FXML TilePane tile = new TilePane();
	private @FXML Button createAlbum;
	private @FXML Button editAlbum;
	private @FXML Button deleteAlbum;
	private @FXML Button openAlbum;
	private @FXML Button search;
	private @FXML Label albumSelectedName;

	private String albumSelected;

	/**
	 * Deletes the album that user selected
	 * 
	 * @param e Click on delete button
	 * @throws IOException Improper user input
	 */
	public void deleteAlbum(ActionEvent e) throws IOException {
		Userbase.getCurrentUser().deleteAlbum(albumSelected);
		albumSelected = null;
		albumSelectedName.setText("None");
		// Userbase.save();
		displayAlbums();
	}

	/**
	 * Creates an album named after what user specifies
	 * 
	 * @param e Click on create album button
	 * @throws IOException Improper user input
	 */
	public void createAlbum(ActionEvent e) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Adding New Album");
		alert.setHeaderText("Enter the album's name below and click 'save' to add it to your library.");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField newName = new TextField();
		newName.setPromptText("Enter album name");

		grid.add(new Label("Album name:"), 0, 1);
		grid.add(newName, 1, 1);
		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Save", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == saveButton) {
			Userbase.getCurrentUser().addAlbum(newName.getText());

		}
		displayAlbums();
	}

	/**
	 * Changes the name of an already existing album
	 * 
	 * @param e User
	 * @throws IOException Improper user input
	 */
	public void editAlbum(ActionEvent e) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Editing album");
		alert.setHeaderText("Click 'Save' to store any changes.");

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField newName = new TextField();
		newName.setText(albumSelected);

		grid.add(new Label("Album name:"), 0, 1);
		grid.add(newName, 1, 1);
		alert.getDialogPane().setContent(grid);

		ButtonType saveButton = new ButtonType("Save", ButtonData.OK_DONE);
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(saveButton, cancelButton);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == saveButton) {
			Userbase.getCurrentUser().changeAlbumName(albumSelected, newName.getText());

		}
		// Userbase.save();
		displayAlbums();
	}

	/**
	 * display the photos in an album
	 * 
	 * @param e Click on open album button
	 * @throws IOException Improper user input
	 */
	public void openAlbum(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/photoGallery.fxml"));
		Pane root = (Pane) loader.load();

		photoGalleryController n = loader.getController();
		n.setCurrentAlbum(Userbase.getCurrentUser().getAlbum(albumSelected));
		n.displayPhotos();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
		stage.setOnHiding((EventHandler<WindowEvent>) new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						try {
							Userbase.save();
							System.out.println("exited");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}
		});
		root.requestFocus();
	}

	/**
	 * Searchs for photos based on user specification
	 * 
	 * @param e User clicks search
	 * @throws IOException Improper user input
	 */
	public void search(ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/search.fxml"));
		Pane root = (Pane) loader.load();

		searchController n = loader.getController();
		n.init();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
		stage.setOnHiding((EventHandler<WindowEvent>) new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						try {
							Userbase.save();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}
		});
		root.requestFocus();

		displayAlbums();
	}

	/**
	 * Displays albums
	 * 
	 * @throws FileNotFoundException file not found
	 */
	public void displayAlbums() throws FileNotFoundException {
		tile.getChildren().clear();
		List<Album> userAlbums = Userbase.getCurrentUser().getAlbums();
		String dateRange;
		for (Album album : userAlbums) {
			File thumbnail;

			if (album.getAlbumSize() == 0) {
				thumbnail = new File("data/emptyFolder.jpeg");
				dateRange = "No dates";
			} else {
				thumbnail = new File(album.getPhotos().get(0).getPath());
				dateRange = album.getDateRange();
			}

			Image image = new Image(new FileInputStream(thumbnail), 100, 100, true, true);
			ImageView imageView = new ImageView(image);
			imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent mouseEvent) {

					if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
						albumSelected();
						albumSelected = album.getName();
						albumSelectedName.setText(albumSelected);

					}

				}
			});
			VBox v = new VBox(5);
			v.getChildren().addAll(imageView, new Text(album.getName()), new Text(String.valueOf(album.getAlbumSize())),
					new Text(dateRange));
			tile.getChildren().add(v);
			tile.setHgap(20);
			tile.setVgap(20);

		}

		initState();
	}

	/**
	 * Logs out user
	 * 
	 * @param e User clickc log out button
	 * @throws IOException file not found
	 */
	public void logOut(ActionEvent e) throws IOException {
		Userbase.save();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/login.fxml"));
		Pane root = (Pane) loader.load();
		Scene scene = new Scene(root);
		Stage stage = loginController.stage;
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Determines what buttons to disable
	 */
	private void initState() {
		editAlbum.setDisable(true);
		deleteAlbum.setDisable(true);
		openAlbum.setDisable(true);
		albumSelectedName.setText("None");

		for (Album album : Userbase.getCurrentUser().getAlbums()) {
			if (album.getPhotos().size() != 0) {
				search.setDisable(false);
				return;
			}

		}

		search.setDisable(true);
	}

	/**
	 * Determines what album the user selected
	 */
	private void albumSelected() {
		editAlbum.setDisable(false);
		deleteAlbum.setDisable(false);
		openAlbum.setDisable(false);
	}

	/**
	 * Displays intro message
	 * 
	 * @throws IOException Imporper user format
	 */
	public void introMessage() throws IOException {
		if (Userbase.getCurrentUser().totalVisits() != 1)
			return;

		Alert alert = new Alert(AlertType.CONFIRMATION);
		
		alert.setTitle("Welcome!");
		alert.setHeaderText("Hi " + Userbase.getCurrentUser().getUsername()
				+ "! \n\nWelcome to Photos!\n\nThis is a one-time informative guide on how Photos works to ensure you get the best experience!");
		BufferedReader br = new BufferedReader(new FileReader("data/introduction.txt"));
		String intro = "";
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			intro = sb.toString();
		} finally {
			br.close();
		}
		
		TextArea text = new TextArea(intro);
		text.setWrapText(true);
		text.setEditable(false);
		
		alert.getDialogPane().setContent(text);
		alert.setContentText(intro);
		alert.setResizable(true);

		ButtonType saveButton = new ButtonType("Got it!", ButtonData.OK_DONE);
		alert.getButtonTypes().setAll(saveButton);
		Optional<ButtonType> result = alert.showAndWait();

	}

	/**
	 * 
	 * saves users photos and quits applicaiton
	 * @param e
	 * ActionEvent for when user clicks quit button
	 * @throws IOException
	 * Throws exception if file read from is invalid
	 */
	public void quit(ActionEvent e) throws IOException {
		Userbase.save();
		Platform.exit();
	}

}
