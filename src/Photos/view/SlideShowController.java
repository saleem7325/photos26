package Photos.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import Photos.app.Album;
import Photos.app.Photo;
import Photos.app.Userbase;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * @author Saleem Khan
 * @author Pauli Peralta
 */
public class SlideShowController {

    private @FXML Button forward;
    private @FXML Button backward;
    private @FXML Button signOut;
    private @FXML Button quit;
    private @FXML Button back;
    private @FXML ImageView imageView;
    private Image image;

    /**
     * Stores the photos in the album
     */
    private List<Photo> photos;

    /**
     * Current album
     */
    private Album album;

    /**
     * slide show index in photos list
     */
    private int index;

    /**
     * Displays the first photo in the album
     * 
     * @param album Album to be displayed
     * @throws FileNotFoundException file not found
     */
    public void initiate(Album album) throws FileNotFoundException {
        this.album = album;
        photos = this.album.getPhotos();
        index = 0;
        image = new Image(new FileInputStream(photos.get(0).getPath()), 350, 300, true, true);
        imageView.setImage(image);
        state();
    }

    /**
     * Displays next photo in album
     * 
     * @param e user clicks forward button
     * @throws IOException invalid user interaction
     */
    public void forward(ActionEvent e) throws IOException {
        image = new Image(new FileInputStream(photos.get(++index).getPath()), 350, 300, true, true);
        imageView.setImage(image);
        state();

    }

    /**
     * Displays previous photo in album
     * 
     * @param e user click backward button
     * @throws IOException invalid user interaction
     */
    public void backward(ActionEvent e) throws IOException {
        image = new Image(new FileInputStream(photos.get(--index).getPath()), 350, 300, true, true);
        imageView.setImage(image);
        state();

    }

    /**
     * logs the user out of the application
     * 
     * @param e user clicks log out
     * @throws IOException invalid user interaction
     */
    public void logOut(ActionEvent e) throws IOException {
        Userbase.save();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/Photos/view/login.fxml"));
        Pane root = (Pane) loader.load();
        Scene scene = new Scene(root);
        Stage stage = loginController.stage;
        stage.setScene(scene);
        stage.show();
    }

    /**
     * quits the application for the user
     * 
     * @param e user clicks quit
     * @throws IOException Invalid user interaction
     */
    public void quit(ActionEvent e) throws IOException {
        Userbase.save();
        Platform.exit();
    }

    /**
     * @param e
     * ActionEvent for when user click back button
     * @throws IOException
     * Invalid file path
     */
    public void back(ActionEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/Photos/view/photoGallery.fxml"));
        Pane root = (Pane) loader.load();

        photoGalleryController n = loader.getController();
        n.setCurrentAlbum(album);
        n.displayPhotos();
        Scene scene = new Scene(root);
        Stage stage = loginController.stage;
        stage.setScene(scene);
        stage.show();
        root.requestFocus();

    }

    private void state() {
        if (index == 0)
            backward.setDisable(true);
        else
            backward.setDisable(false);

        if (index == photos.size() - 1)
            forward.setDisable(true);
        else
            forward.setDisable(false);
    }

}
