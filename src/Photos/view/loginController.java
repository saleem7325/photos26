package Photos.view;

import java.io.IOException;



import Photos.app.Userbase;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


/**
 * 
 * @author Pauli Peralta
 * @author Saleem Khan
 *
 */
public class loginController {
	private @FXML TextField user;
	/**
	 * holds stage insstance
	 */
	public static Stage stage;
	private Scene scene;
	
	/**
	 * 
	 * @param mainStage
	 * Stage
	 */

	public void start(Stage mainStage) {
		stage = mainStage;
	}
	

	/**
	 * 
	 * @param e
	 * ActionEvent detects when login button is clicked
	 * @throws IOException
	 * Attempts to read an external file. Throws error if unable to read or write to the file.
	 */
	public void userLogin(ActionEvent e) throws IOException {

		String username = user.getText();
		if(username.trim().isEmpty()) return;
		if (Userbase.verifyLoginInfo(username)) {
			if (username.equals("admin"))
				displayAdminView();
			else {
				displayNonAdminView();
			}
		}else {
			displayErrorMessage(
					username + " does not exist",
					"Inavalid Username");
		}


	}

	/**
	 * Changes scene to admin's view
	 * 
	 * @throws IOException
	 * Attempts to read an external file. Throws error if unable to read or write to the file.
	 */
	private void displayAdminView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/admin.fxml"));
		Pane root = (Pane) loader.load();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
		stage.setOnHiding((EventHandler<WindowEvent>) new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                       try {
						Userbase.save();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    }
                });
            }
        });
	}

	/**
	 * Changes scene to non admin's view
	 * 
	 * @throws IOException
	 * Attempts to read an external file. Throws error if unable to read or write to the file.
	 */
	private void displayNonAdminView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/Photos/view/nonAdmin.fxml"));
		Pane root = (Pane) loader.load();

		nonAdminController n = loader.getController();
		n.displayAlbums();
		n.introMessage();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
		stage.setOnHiding((EventHandler<WindowEvent>) new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                       try {
						Userbase.save();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    }
                });
            }
        });
		root.requestFocus();

	}
	
	/**
	 * Handles when user clicks on the quit to logout.
	 * 
	 * @param e
	 * AciotnEvent argument for when user clicks on the 'quit' button.
	 * @throws IOException
	 * Makes call to a method that reads from an external file. Exception is throw if file reading/writing fails.
	 */
	public void quit(ActionEvent e) throws IOException {
		Userbase.save();
		Platform.exit();
	}
	
	
	/**
	 * Display an error alert to the user.
	 * 
	 * @param message
	 * Message to display.
	 * @param title
	 * Title of alert.
	 */
	private void displayErrorMessage(String message, String title) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

	}
}
