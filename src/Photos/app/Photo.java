package Photos.app;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

/**
 * Photo class contains the file of a photo along with other attributes
 * pertaining to the photo. Photo also provides the ability to add/remove tags
 * to/from photos and caption photos.
 * 
 * @author Saleem Khan
 * @author Pauli Peralta
 */
public class Photo implements Serializable {

	/**
	 * Saves photo file
	 */
    private File photo;
    
    /**
     * saves date of last modification
     */
    private String date;
    
    /**
     * saves caption
     */
    private String caption;
    
    /**
     * saves last modification date as localDate
     */
    private LocalDate timeTaken;

    /**
     * saves tags
     */
    private Map<String, List<String>> tags;

    /**
     * initiates new photo with indicated path and caption
     * @param path
     * path to assign to photo
     * @param caption
     * caption to assign to photo
     */
    public Photo(String path, String caption) {
        photo = new File(path);

        Calendar c = Calendar.getInstance();
        c.clear();
        c.setTimeInMillis(photo.lastModified());
        c.set(Calendar.MILLISECOND, 0);

        date = (c.get(Calendar.MONTH)) + "/" + c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.YEAR);
        String day = "";
        String month = "";

        if (c.get(Calendar.DAY_OF_MONTH) < 10)
            day = "0" + String.valueOf(c.get(Calendar.DAY_OF_MONTH));
        else
            day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));

        if (c.get(Calendar.MONTH) + 1 < 10)
            month = "0" + String.valueOf(c.get(Calendar.MONTH) + 1);
        else
            month = String.valueOf(c.get(Calendar.MONTH) + 1);

        timeTaken = LocalDate.parse(c.get(Calendar.YEAR) + "-" + month + "-" + day);
        date = timeTaken.toString();

        this.caption = caption;
        tags = new HashMap<>();
    }

    /**
     * returns the time the photo was taken
     * 
     * @return time photo was taken
     */
    public LocalDate getTime() {
        return timeTaken;
    }

    /**
     * returns the name of the photo file
     * 
     * @return name of file
     */
    public String getName() {
        return photo.getName();
    }

    /**
     * Returns a Map of tags pertaining to photo
     * 
     * @return Map containig tags
     */
    public Map<String, List<String>> getTags() {
        return tags;
    }

    /**
     * Returns instance of File pertaining to photo
     * 
     * @return File of photo
     */
    public File getFile() {
        return photo;
    }

    /**
     * Adds the key and value to tags
     * 
     * @param key   key
     * @param value value
     */
    public void addTag(String key, String value) {
        if (key.trim().isEmpty() || value.trim().isEmpty())
            return;
        tags.putIfAbsent(key, new ArrayList<>());
        tags.get(key).add(value);
    }

    /**
     * removes key and value if contained in map
     * 
     * @param key   key to remove
     * @param value value to remove
     * @return true if removed, false if tags doesn't contain key
     */
    public boolean removeTag(String key, String value) {
        if (!tags.containsKey(key))
            return false;

        for (String val : tags.get(key)) {
            if (val.equals(value)) {
                tags.get(key).remove(val);
                if (tags.get(key).size() == 0)
                    tags.remove(key);
                return true;
            }
        }

        return false;
    }

    /**
     * Determines if a key value pair is in tags
     * 
     * @param name  key
     * @param value value
     * @return true if key value pair is present, false if it is not
     */
    public boolean findTag(String name, String value) {
        if (!tags.containsKey(name))
            return false;

        for (String val : tags.get(name)) {
            if (val.equals(value))
                return true;
        }

        return false;
    }

    /**
     * Returns date the photo was taken
     * 
     * @return date photo was taken
     */
    public String getDate() {
        return date;
    }

    /**
     * Returns caption pertaining to photo instance
     * 
     * @return caption of photo
     */
    public String getCaption() {
        return caption;
    }

    /**
     * Sets caption to String argument
     * 
     * @param newCaption new Caption of photo
     */
    public void setCaption(String newCaption) {
        caption = newCaption;
    }

    /**
     * Returns absolute path of photo file
     * 
     * @return absolute path of photo file
     */
    public String getPath() {
        return photo.getAbsolutePath();
    }

}
