package Photos.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;



/**
 * @author Pauli Peralta
 * @author Saleem Khan
 * 
 */

public class User implements Serializable {

	/**
	 * Holds the username of a user.
	 */
	private String username;

	/**
	 * Holds the list of albums that a user has created.
	 */
	private List<Album> albums;

	/**
	 * Tracks the number of times a user has logged in.
	 */
	private int visits;

	/**
	 * 
	 * @param username
	 * Initiates a User instance with the specified username.
	 */
	public User(String username) {
		this.username = username;
		albums = new ArrayList<>();
		visits = 0;
	}

	/**
	 * Returns a list of the albums a user has access to.
	 * 
	 * @return
	 * Returns a list of the albums a user has access to.
	 */
	public List<Album> getAlbums() {
		return albums;
	}

	/**
	 * Attempts to create and add an album to a user's album list.
	 * 
	 * @param name
	 * Name of the album to be added.
	 * 
	 * @return boolean
	 * If the specified name is an empty string or already exists, returns false.
	 * If the specified name is not empty and is unique, returns true.
	 * 
	 * @throws IOException
	 * Makes a call to a function that reads from another file. If for some reason the file can't be read, this exception is thrown.
	 */
	public boolean addAlbum(String name) throws IOException {
		if (name.trim().isEmpty()) {
			displayErrorMessage("Album names cannot be empty.", "Invalid Album Name Error");
			return false;
		}

		if (getAlbum(name) == null) {
			Album newAlbum = new Album(name);
			albums.add(newAlbum);
			if(username.equals("stock") && timeAppStarted() == 1) return true;
			displayConfirmationMessage(name + " album was successfully added!", "Success!");
			return true;
		} else {
			displayErrorMessage("Album name already exists. Duplicate albums are not allowed", "Duplicate Album Error");
			return false;
		}
	}

	/**
	 * Deletes the album that matches the argument name passed in.
	 * 
	 * @param albumName
	 * The name of the album to be deleted.
	 */
	public void deleteAlbum(String albumName) {
		int indexFound = 0;

		for (int i = 0; i < albums.size(); i++) {
			if (albums.get(i).getName().equals(albumName)) {
				indexFound = i;
				break;
			}
		}

		albums.remove(indexFound);
	}

	/**
	 * Fetches an Album instance from user's album list that matches the name passed in as the argument.
	 * 
	 * @param albumName
	 * The name of the album to be returned.
	 * @return
	 * If a match is found, the Album instance is returned, else null is returned.
	 */
	public Album getAlbum(String albumName) {
		for (Album album : albums) {
			if (album.getName().equals(albumName))
				return album;
		}

		return null;
	}

	/**
	 * Returns a user's username.
	 * 
	 * @return
	 * Returns a user's username.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Gets the total amount a user has logged in.
	 * 
	 * @return
	 * Returns the total amount a user has logged in.
	 */
	public int totalVisits() {
		return visits;
	}

	/**
	 * Sets the total amount a user has logged in.
	 * 
	 * @param n
	 * Sets the total amount a user has logged in.
	 */
	public void setVisits(int n) {
		visits = n;
	}

	/**
	 * Looks through the user's album list for an album named initialName, if it exists, its name is changed to to newName.
	 * 
	 * @param initialName
	 * Indicates the initial name of an album.
	 * @param newName
	 * Indicates the new name of an album.
	 */
	public void changeAlbumName(String initialName, String newName) {
		if (initialName.equals(newName))
			return;
		if (getAlbum(newName) != null) {
			displayErrorMessage("Photo already exists. Duplicate photos in an album are not allowed",
					"Duplicate Photo Error");
			return;
		}

		for (Album album : albums) {
			if (album.getName().equals(initialName))
				album.setName(newName);
		}
	}

	/**
	 * Display an error alert to the user.
	 * 
	 * @param message
	 * Message to be displayed on the alert.
	 * @param title
	 * Title of the alert.
	 */

	private void displayErrorMessage(String message, String title) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

	}

	/**
	 * Displays a confirmation alert to the user.
	 * 
	 * @param message
	 * Message to be displayed on the alert.
	 * @param title
	 * Title of the alert.
	 */

	private static void displayConfirmationMessage(String message, String title) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

	}

	/**
	 * Returns the user's username
	 */
	public String toString() {
		return getUsername();
	}

	/**
	 * Returns the number of times the application has been started.
	 * 
	 * @return
	 * Returns the number of times the application has been started.
	 * @throws IOException
	 * Attmepts to read from a file. If file not found, exception is thrown.
	 */
	private static int timeAppStarted() throws IOException {
		Scanner scan = new Scanner(new File("data/timesStarted.txt"));
		return Integer.parseInt(scan.next());

	}


}