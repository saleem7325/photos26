
package Photos.app;

import java.io.Serializable;

/**
 * 
 * @author Pauli Peralta
 * @author Saleem Khan
 * 
 *
 */

public class Tag implements Serializable {
	/**
	 * holds the name of the tag.
	 */
	private String name;

	/**
	 * holds the value of the tag.
	 */
	private String value;



	/**
	 * Initiates a tag object with the indicated name and value
	 * 
	 * @param name
	 * indicates the name to be assigned to the new tag
	 * @param value
	 * indicates the value to be assigned to the new tag
	 */
	public Tag(String name, String value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * 
	 * @return
	 * returns the value of the tag
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 
	 * @return
	 * returns the name of the tag
	 */
	public String getName() {
		return name;
	}
}
