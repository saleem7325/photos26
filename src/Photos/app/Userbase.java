package Photos.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * @author Pauli Peralta
 * @author Saleem Khan
 */


public class Userbase {

	/**
	 * holds the list of all users
	 */
	private static List<User> activeUsers = null;
	
	/**
	 * relative path to the file where the data is saved to
	 */
	private static File STORAGE = new File("data/Users.txt");
	
	/**
	 * relative path to the stock album
	 */
	private static String STOCK_ALBUM = "data/stockAlbum";

	/**
	 * holds the User that's currently using the applcation
	 */
	private static User currentUser;

	/**
	 * initiates a Userbase object. Only initiates the activeUsers list once
	 */
	public Userbase() {
		if (activeUsers == null)
			activeUsers = new ArrayList<>();

	}

	/**
	 * Attempts to add username to the list of active users.
	 * 
	 * @param username
	 * Indicates the name of the user to be added.
	 * @return
	 * If the username is not an empty string and is unique with respect to the other active users, this method returns true; otherwise, 
	 * it returns false.
	 * @throws IOException
	 * Makes a call to a another method that reads from a file. If file can't be read, the exception is thrown.
	 */
	public static boolean addUser(String username) throws IOException {
		// TODO: show pop up message letting user know if add was a success or not
		if (username.trim().isEmpty()) {
			displayErrorMessage("Username cannot be empty!", "Invalid Username");
			return false;
		}

		if(username.equals("admin")) {
			displayErrorMessage("You already exist! Therefore you can't add yourself, sorry!", "Invalid Username");
			return false;
		}

		// duplicate case
		for (User user : activeUsers) {
			if (user.getUsername().equals(username)) {
				displayErrorMessage("This user already exists! No duplicates allowed.", "Invalid Username");
				return false;
			}
		}

		User user = new User(username);
		activeUsers.add(user);
		if (username.equals("stock"))
			loadStockAlbum(user);

		
		displayConfirmationMessage("User " + username + " has been successfully added!", "New User Added");
		return true;
	}

	/**
	 * Attempts to remove username from the list of active users.
	 * 
	 * @param username
	 * Indicates the name of the user to be removed.
	 * @return
	 * If the username matches the name of a user on active user list, then the user is deleted and the method returns true: otherwise, it returns false.
	 * @throws IOException
	 * Makes a call to a another method that reads from a file. If file can't be read, the exception is thrown.
	 */
	public static boolean removeUser(String username) throws IOException {
		if (username.trim().isEmpty()) {
			displayErrorMessage("No username was indicated, no one was deleted!", "Invalid Username");
			return false;
		}


		if(username.equals("admin")) {
			displayErrorMessage("This is kind of an important user, we can't let you delete this one, sorry!", "Invalid Username");
			return false;
		}

		int indexDelete = -1;
		for (int i = 0; i < activeUsers.size(); i++) {
			if (username.equals(activeUsers.get(i).getUsername())) {
				indexDelete = i;
				break;
			}
		}
		if (indexDelete != -1) {
			activeUsers.remove(indexDelete);
			displayConfirmationMessage(username + " has been deleted!", "User Deleted");
			return true;
		}

		displayErrorMessage("Sorry, the username " + username + " wasn't deleted because it doesn't exist", "Invalid Username");
		return false;
	}

	/**
	 * Returns the list of active users
	 * 
	 * @return
	 * Returns a list of User instances. This list represents that list of all active users.
	 */
	public static List<User> getActiveUsers() {
		return activeUsers;
	}

	/**
	 * Verifies if username is in the list of active users.
	 * 
	 * @param username
	 * Name of the user to be verified.
	 * @return
	 * If username matches the name of a User instance in the active user list the method returns true; otherwise, it returns false.
	 */
	public static boolean verifyLoginInfo(String username) {
		if (username.equals("admin"))
			return true;
		else {

			for (User user : activeUsers) {
				if (username.equals(user.getUsername())) {
					currentUser = user;
					currentUser.setVisits(currentUser.totalVisits() + 1);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns the User object of the user currently using the application.
	 * 
	 * @return
	 * Returns the User object of the user currently using the application.
	 */
	public static User getCurrentUser() {
		return currentUser;
	}

	/**
	 * Stores the current state of the program into an external file for persistence.
	 * 
	 * @throws IOException
	 * Attempts to read an external file. Throws error if unable to read or write to the file.
	 */
	public static void save() throws IOException {
		STORAGE.delete();
		STORAGE = new File("data/Users.txt");
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(STORAGE));
		for (User user : activeUsers) {
			out.writeObject(user);
			out.flush();
		}
	}

	/**
	 * Loads the stock album to a user's gallery.
	 * 
	 * @param user
	 * Indicates the name of the user onto which to load the stock album onto.
	 * @throws IOException
	 * Attempts to read an external file. Throws error if unable to read or write to the file.
	 */
	private static void loadStockAlbum(User user) throws IOException {
		user.addAlbum("stock");
		File folder = new File(STOCK_ALBUM);
		for (File file : folder.listFiles()) {
			String path = file.getAbsolutePath();
			String[] ext = path.split("\\.");
			user.getAlbum("stock").addPhoto(new Photo(path, "stock " + ext[1]));
		}
	}

	
	/**
	 * Display an error alert to the user.
	 * 
	 * @param message
	 * Message to display.
	 * @param title
	 * Title of alert.
	 */
	private static void displayErrorMessage(String message, String title) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

	}
	
	/**
	 * Displays a confirmation alert to the user.
	 * 
	 * @param message
	 * Message to display.
	 * @param title
	 * Title of alert.
	 */

	private static void displayConfirmationMessage(String message, String title) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

	}

	/**
	 * Returns the number of times the application has been started.
	 * 
	 * @return
	 * Returns the number of times the application has been started.
	 * @throws IOException
	 * Attempts to read an external file. Throws error if unable to read or write to the file.
	 */
	private static int timeAppStarted() throws IOException {
		Scanner scan = new Scanner(new File("data/timesStarted.txt"));
		return Integer.parseInt(scan.next());

	}



}
