package Photos.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Scanner;

import Photos.view.loginController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * @author Saleem Khan
 * @author Pauli Peralta
 */
public class Photos extends Application {

    private static final File STORAGE = new File("data/Users.txt");

    /**
     * Starts up photos application
     */
    public void start(Stage primaryStage) throws Exception {
        loadUserData();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/Photos/view/login.fxml"));
        Pane root = (Pane) loader.load();
        loginController loginController = loader.getController();
        loginController.start(primaryStage);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Photos");
        primaryStage.setResizable(false);
        primaryStage.show();

        primaryStage.setOnHiding((EventHandler<WindowEvent>) new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Userbase.save();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        if (timeAppStarted() == 0)
            addStock();
    }

    /**
     * Launches photos application
     * 
     * @param args
     * arguments
     */
    public static void main(String[] args) {
        launch(args);

    }

    /**
     * Reads in User data from users.txt
     * 
     * @throws IOException Invalid file data
     */
    private void loadUserData() throws IOException {
        Userbase userbase = new Userbase();
        FileInputStream fin = new FileInputStream(STORAGE);
        ObjectInputStream in = new ObjectInputStream(fin);

        try {
            while (fin.available() > 0) {
                Userbase.getActiveUsers().add((User) in.readObject());
            }
        } catch (Exception e) {
            System.out.println("Could not load user data!");
        }
    }

    /**
     * Adds the stock user
     */
    private void addStock() throws IOException {
        Userbase.addUser("stock");
    }

    /**
     * Reads the number of time the app was started from timesStarted.txt
     * 
     * @return the number of times photos has been launched
     * @throws IOException improper file data
     */
    private int timeAppStarted() throws IOException {
        Scanner scan = new Scanner(new File("data/timesStarted.txt"));
        int num = Integer.parseInt(scan.next());

        BufferedWriter out = new BufferedWriter(new FileWriter("data/timesStarted.txt"));
        out.write(String.valueOf(num + 1));
        out.close();

        return num;

    }

}
