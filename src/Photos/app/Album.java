package Photos.app;

import java.io.Serializable;
import java.util.*;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * The Album class keeps track of all the photos that are contained within the
 * album instance, Album also provides the ability to add/remove photos and to
 * retrieve relevant information pertaining to a particular Album instance.
 * 
 * @author Saleem Khan
 * @author Pauli Peralta
 */
public class Album implements Serializable {

    /**
     * Stores the name of the album
     */
    private String name;

    /**
     * Stores all the photos in the album
     */
    private List<Photo> photos;

    public Album(String name) {
        this.name = name;
        photos = new ArrayList<Photo>();
    }

    /**
     * Checks if Photo instance is in the album, if not photo gets added to album
     * then the list of photos is sorted to keep photos in chronological order.
     * 
     * @param photo Photo to add, if photo is already in the album the photo will
     *              not be added.
     */
    public void addPhoto(Photo photo) {
        if (getPhoto(photo.getPath()) != null) {
            displayErrorMessage("Photo already exists. Duplicate photos in an album are not allowed",
                    "Duplicate Photo Error");
            return;
        }

        photos.add(photo);
        photos.sort((Photo p, Photo i) -> {
            return p.getTime().compareTo(i.getTime());
        });
    }

    /**
     * Removes the photo parameter from list
     * 
     * @param photo Photo to remove from list of photos
     */
    public void removePhoto(Photo photo) {
        photos.remove(photo);
    }

    /**
     * Returns the current number of photos in list
     * 
     * @return number of photos in list
     */
    public int getAlbumSize() {
        return photos.size();
    }

    /**
     * Returns a string contaning the earliest date a photo in the album was taken
     * and the latest date a photo in the album was taken
     * 
     * @return a string with earliest date and latest date
     */
    public String getDateRange() {

        String earliestDate = photos.get(0).getDate();
        String latestDate = photos.get(photos.size() - 1).getDate();

        return "[" + earliestDate + " → " + latestDate + "]";
    }

    /**
     * Sets the name of the album to the String parameter
     * 
     * @param name new name of Album instance
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the name of the Album instance
     * 
     * @return name of Album instance
     */
    public String getName() {
        return name;
    }

    /**
     * Checks if any photos path match the parameter path if an instance exists in
     * the album that instance of Photo is returned else null
     * 
     * @param photoPath path of file
     * @return instance of Photo with path mathing param or null
     */
    public Photo getPhoto(String photoPath) {
        for (Photo photo : photos) {
            if (photo.getPath().equals(photoPath))
                return photo;
        }

        return null;
    }

    /**
     * Returns a list of photos in Album
     * 
     * @return List of photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * Displays an Alert message to user containing message and title
     * 
     * @param message Message inside of Alert
     * @param title   Title of Error message window
     */
    private void displayErrorMessage(String message, String title) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(message);
        alert.showAndWait();

    }

}
